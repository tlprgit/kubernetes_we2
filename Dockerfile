FROM tomcat
MAINTAINER tlpr 123
COPY target/mvn-hello-world.war /usr/local/tomcat/webapps
USER root
WORKDIR /usr/local/tomcat/webapps/
EXPOSE 8080
CMD ["catalina.sh","run"]
